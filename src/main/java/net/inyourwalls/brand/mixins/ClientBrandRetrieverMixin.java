// ClientBrandRetrieverMixin: The only code in this mod!
package net.inyourwalls.brand.mixins;

import net.minecraft.client.ClientBrandRetriever;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ClientBrandRetriever.class)
public class ClientBrandRetrieverMixin {
    @Inject(at = @At("RETURN"), method = "getClientModName", cancellable = true, remap = false)
    private static void returnCustomClientBrand(CallbackInfoReturnable<String> cir) {
        cir.setReturnValue("inyourwalls.net");
    }
}
