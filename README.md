# CustomBrand

Mod I use to change my Minecraft client brand. If you want to change the
brand, you will have to edit the code. I *strongly* recommend finding an
alternative that is intended for end users.
