plugins {
    java
    id("fabric-loom") version "1.4.5"
}

group = "net.inyourwalls"
version = "1.0.0"

dependencies {
    val minecraftVersion = "1.20.4"
    val yarnBuild = "3"
    val fabricLoaderVersion = "0.15.0"

    minecraft("com.mojang:minecraft:$minecraftVersion")
    mappings("net.fabricmc:yarn:$minecraftVersion+build.$yarnBuild:v2")
    modImplementation("net.fabricmc:fabric-loader:$fabricLoaderVersion")
}

tasks.processResources {
    inputs.property("version", project.version)
    filesMatching("fabric.mod.json") {
        expand(mapOf("version" to project.version))
    }
}
